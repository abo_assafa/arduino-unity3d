﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Threading;

public class ArduinoJoystick
{
    // format 
    public float coordinate_x, coordinate_y, coordinate_z;
    public int joystick_x, joystick_y;
    public bool joystick_button, left_button, right_button;

    private static ArduinoJoystick singleton = null;
    private StreamReader serialPort;
    private ArduinoJoystick()
    {
        try
        {
            //for unix based sys
            serialPort = new StreamReader("/dev/cu.HC-06-SPPDev");
        }
        catch (IOException e)
        {
            Debug.LogError("Couldn't connect to arduino controller, error:" + e.Message );
        }
    }

    public static ArduinoJoystick GetSingleton()
    {
        if (singleton == null)
        {
            singleton = new ArduinoJoystick();
        }
        return singleton;
    }

    public void Update()
    {
        string value = "";
        try
        {
            value= this.serialPort.ReadLine();
        } catch (Exception e){
            Debug.LogError("Connection lost! retrying..., error:" + e.Message);
        }

        string[] input = value.Split(',');
        if (input.Length < 8)
        {
            return;
        }
        if (
            input[0] != "" || input[1] != "" || input[2] != "" || 
            input[3] != "" || input[4] != "" || input[5] != "" ||
            input[6] != "" || input[7] != ""
           )
        {
            singleton.coordinate_x = float.Parse(input[0]);
            singleton.coordinate_y = float.Parse(input[1]);
            singleton.coordinate_z = float.Parse(input[2]);
            singleton.joystick_x = int.Parse(input[3]);
            singleton.joystick_y = int.Parse(input[4]);
            singleton.joystick_button = input[5] == "1";
            singleton.left_button = input[6] == "1";
            singleton.right_button = input[7] == "1";
            singleton.serialPort.BaseStream.Flush();
        }
    }
}

