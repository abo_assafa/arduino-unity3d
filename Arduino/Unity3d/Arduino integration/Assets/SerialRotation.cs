﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class SerialRotation : MonoBehaviour
{
    ArduinoJoystick controller = ArduinoJoystick.GetSingleton();
    float[] lastRot = { 0, 0, 0 };
    void Update()
    {
        controller.Update();
        //transform.SetPositionAndRotation(transform.position, new Quaternion(/5.3f,
                                                                            ///5.3f,
                                                                            /// 5.3f
                                                                            //,1));
        transform.Rotate(    //Rotate the camera based on the new values
                         -25 * (controller.coordinate_y - lastRot[0]),
                       lastRot[1],
                       lastRot[2],
                        Space.Self
                        );
        lastRot[0] = controller.coordinate_y;  //Set new values to last time values for the next loop
        lastRot[1] = 5 * controller.coordinate_z;
        lastRot[2] = 5 * controller.coordinate_y;
    }

    void OnGUI()
    {
        string newString = "Connected: " + transform.rotation.x + ", " + transform.rotation.y + ", " + transform.rotation.z;
        GUI.Label(new Rect(10, 10, 300, 100), newString); //Display new values
        // Though, it seems that it outputs the value in percentage O-o I don't know why.
    }
}