

#include <SoftwareSerial.h>

SoftwareSerial MySerial(10,11); //(rxPin, txPin);

void setup() {
  MySerial.begin(9600); // 9600 bps
}

void loop() {
  MySerial.println("Bluetooth modülünden mesaj.");
}

int JoyStick_X = 0; //x
int JoyStick_Y = 1; //y
int JoyStick_Z = 3; //key
int l_inpin=7;// initialize pin 7
int r_inpin=8;// initialize pin 8
int left,right;

#include <Wire.h>
// Wire'ı kullanabilmemiz için ADXL345 sensoru için Register tanımlıyoruz
#define ADXL345_ADDRESS (0xA6 >> 1)
                                     // Wire kütüphanesi 7 bitlik adres alır
                                     // (0xA6 >> 1)  => '1010011'
#define ADXL345_REGISTER_XLSB (0x32)

int accelerometer_data[3];
// void because this only tells the cip to send data to its output register

// Sensorun buffer'ına veri yazar.
void i2c_write(int address, byte reg, byte data) {
  // Transferi başlatması için Wire'e Sensorun output registerinin adresini veriyoruz
  Wire.beginTransmission(address);
  // Bağlantı kuruluyor
  Wire.write(reg);
  // Veri gönderiliyor.
  Wire.write(data); //low byte
  Wire.endTransmission(); //bağlantıyı kapatıyoruz
}

// Mikrokontroler sensörün input registerinden veri okur
void i2c_read(int address, byte reg, int count, byte* data) {
  // Okunan byte sayısını tutmak için
  int i = 0;
  // Transferi başlatması için Wire'e Sensorun output registerinin adresini veriyoruz
  Wire.beginTransmission(address);
  // Bağlantı kuruluyor
  Wire.write(reg);
   //bağlantıyı kapatıyoruz
  Wire.endTransmission();

  Wire.beginTransmission(address);
  // Sensörden veri iste
  // Count: istenen byte sayısı
  Wire.requestFrom(address, count);
  while(Wire.available()) // okunabilecek byte sayısını kontrol eder
  {
    char c = Wire.read(); // veriyi byte byte okuyor 
    data[i] = c;
    i++;
  }
  Wire.endTransmission();
}

void init_adxl345() {
  byte data = 0;

  i2c_write(ADXL345_ADDRESS, 0x31, 0x0B);   // 13-bit mode  +_ 16g
  i2c_write(ADXL345_ADDRESS, 0x2D, 0x08);   // Power register

  i2c_write(ADXL345_ADDRESS, 0x1E, 0x00);   // x
  i2c_write(ADXL345_ADDRESS, 0x1F, 0x00);   // Y
  i2c_write(ADXL345_ADDRESS, 0x20, 0x05);   // Z
 
  i2c_read(ADXL345_ADDRESS, 0X00, 1, &data);
  if(data==0xE5)
    Serial.println("Başarılı bağlantı");
  else
    Serial.println("Başarısız bağlantı");
}

void read_adxl345() {
  byte bytes[6];
  memset(bytes,0,6);

  // sensorden 6 byte oku
  i2c_read(ADXL345_ADDRESS, ADXL345_REGISTER_XLSB, 6, bytes);
  // okunan bytelari integere yerlestir
  for (int i=0;i<3;++i) {
    accelerometer_data[i] = (int)bytes[2*i] + (((int)bytes[2*i + 1]) << 8);
  }
}


void setup() 
{
  Wire.begin();
  MySerial.begin(9600); // 9600 bps
  Serial.begin(9600); // 9600 bps
  for(int i=0; i<3; ++i) {
    accelerometer_data[i]  = 0;
  }
  init_adxl345();
}


void loop() 
{
  int x,y,z;
  x=analogRead(JoyStick_X);
  y=analogRead(JoyStick_Y);
  z=digitalRead(JoyStick_Z);
  left=digitalRead(l_inpin);
  right=digitalRead(r_inpin);
  read_adxl345();
  MySerial.flush();
  MySerial.print(float(accelerometer_data[0])*3.9/1000);
  MySerial.print(",");
  MySerial.print(float(accelerometer_data[1])*3.9/1000);
  MySerial.print(",");
  MySerial.print(float(accelerometer_data[2])*3.9/1000);
  MySerial.print(",");
  MySerial.print(x ,DEC);
  MySerial.print(",");
  MySerial.print(y ,DEC);
  MySerial.print(",");
  MySerial.print(z ,DEC);
  MySerial.print(",");
  MySerial.print(left ,DEC);
  MySerial.print(",");
  MySerial.println(right ,DEC);
  delay(100);
}
